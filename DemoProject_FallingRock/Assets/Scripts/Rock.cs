using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{
    public float moveSpeed;

    Rigidbody2D m_rb;

    bool m_isGrounded;

    public bool IsGrounded { get => m_isGrounded; }

    private void Awake()
    {
        m_rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (m_rb)
            m_rb.velocity = Vector3.down * moveSpeed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            m_isGrounded = true;
            Destroy(gameObject, 0.5f);

            GameManager.Ins.Score++;

            GameGUIManager.Ins.UpdateScoreCounting(GameManager.Ins.Score);
            GameGUIManager.Ins.MoveNextScene(GameManager.Ins.Score);
            AudioController.Ins.PlaySound(AudioController.Ins.landSound);

        }
    }
}
