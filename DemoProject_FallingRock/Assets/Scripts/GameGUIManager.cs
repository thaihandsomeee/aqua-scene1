using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameGUIManager : Singleton<GameGUIManager>
{
    public GameObject homeGui;
    public GameObject gameGui;
    public GameObject gameover;

    public Text scoreCountingText;
    public override void Awake()
    {
        MakeSingleton(false);
    }

    public void ShowGameGui(bool iShow)
    {
        if (gameGui)
            gameGui.SetActive(iShow); 

        if (homeGui)
            homeGui.SetActive(!iShow);
    }

    public void ShowGameover(bool iShow)
    {
        if(gameover)
            gameover.SetActive(iShow);
    }

    public void UpdateScoreCounting(int score)
    {
        if (scoreCountingText)
            scoreCountingText.text = "SCORE: x" + score.ToString("000");
    }

    public void MoveNextScene(int score)
    {
        if(score == 10)
        {
            StartCoroutine(Wait());
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(1);
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Home()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ExitGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        Application.Quit();
    }
}
