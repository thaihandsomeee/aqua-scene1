using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boom : MonoBehaviour
{
    public float moveSpeed;

    Rigidbody2D m_rb;
    Animator m_animator;
    bool m_isGrounded;

    public bool IsGrounded { get => m_isGrounded; }

    private void Awake()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        if (m_rb)
            m_rb.velocity = Vector3.down * moveSpeed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            m_animator.SetTrigger("isExplode");
            m_isGrounded = true;
            Destroy(gameObject, 1f);

            GameManager.Ins.Score++;

            GameGUIManager.Ins.UpdateScoreCounting(GameManager.Ins.Score);
            AudioController.Ins.PlaySound(AudioController.Ins.landSound);

        }
    }
}
