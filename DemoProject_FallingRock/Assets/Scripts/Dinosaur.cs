using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dinosaur : MonoBehaviour
{
    public float moveSpeed;

    Rigidbody2D m_rb;

    bool m_isGrounded;

    public bool IsGrounded { get => m_isGrounded; }

    private void Awake()
    {
        m_rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (m_rb)
            m_rb.velocity = Vector3.right * moveSpeed;
    }
}
