using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Hero : MonoBehaviour
{
    public float moveSpeed;
    Rigidbody2D m_rb;
    Animator m_animator;
    bool m_isDead;
    //jumping
    public float jumpSpeed;
    public new Collider2D collider;
    public LayerMask groundLayer;
    bool m_isGrounded;
    //moving platform only
    public GameObject player;

    Vector2 localScale;

    private void Awake()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
    }

    private void Update()
    {
        Flip();
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            transform.position = new Vector3(
        Mathf.Clamp(transform.position.x, -9f, 9f),
        transform.position.y,
        transform.position.z
        );
        }
    }

    private void FixedUpdate()
    {
        if (m_isDead && m_rb)
            m_rb.velocity = new Vector2(0f, m_rb.velocity.y);

        if (m_isDead || !GameManager.Ins.IsGamebegun) return;

        MoveHandler();
    }

    void MoveHandler()
    {
        //jumping
        m_isGrounded = collider.IsTouchingLayers(groundLayer);
        if (GamePadsController.Ins.CanMoveUp && m_isGrounded)
        {
            if (m_rb)
                m_rb.velocity = new Vector2(m_rb.velocity.x, 1f) * jumpSpeed;
            if (m_animator)
            {
                m_animator.SetBool("isJump", true);
            }
        }
        //move left
        if (GamePadsController.Ins.CanMoveLeft)
        {
            if (m_rb)
                m_rb.velocity = new Vector2(-1f * moveSpeed, m_rb.velocity.y);
            if (m_animator && m_isGrounded)
            {
                m_animator.SetBool("isRun", true);
                m_animator.SetBool("isIdle", false);
            }
        }
        //move right
        else if (GamePadsController.Ins.CanMoveRight)
        {
            if (m_rb)
                m_rb.velocity = new Vector2(1f * moveSpeed, m_rb.velocity.y);
            if (m_animator && m_isGrounded)
            {
                m_animator.SetBool("isRun", true);
                m_animator.SetBool("isIdle", false);
            }
        }
        //idle
        else
        {
            if (m_rb)
                m_rb.velocity = new Vector2(0f, m_rb.velocity.y);
            if (m_animator)
            {
                m_animator.SetBool("isIdle", true);
                m_animator.SetBool("isRun", false);
                m_animator.SetBool("isJump", false);
            }
        }
    }

    void Flip()
    {
        localScale = transform.localScale;
        if (GamePadsController.Ins.CanMoveLeft)
        {
            if (localScale.x > 0f)
            {
                localScale.x *= -1.0f;
                transform.localScale = localScale;
            }
        }
        if (GamePadsController.Ins.CanMoveRight)
        {
            if (localScale.x < 0f)
            {
                localScale.x *= -1.0f;
                transform.localScale = localScale;
            }
        }
    }

    void Die()
    {
        m_isDead = true;

        if (m_animator)
            m_animator.SetTrigger("isDead");

        if (m_rb)
            m_rb.velocity = new Vector2(0f, m_rb.velocity.y);

        Destroy(gameObject, 1.5f);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Rock"))
        {
            if (m_isDead) return;

            Rock rock = collision.gameObject.GetComponent<Rock>();

            if (rock && !rock.IsGrounded)
            {
                Die();

                GameManager.Ins.IsGameover = true;

                GameGUIManager.Ins.ShowGameover(true);
                AudioController.Ins.PlaySound(AudioController.Ins.landSound);
                AudioController.Ins.PlaySound(AudioController.Ins.loseSound);
            }
        }
        //platform movement
        if (collision.gameObject.CompareTag("Platform"))
        {
            player.transform.parent = collision.gameObject.transform;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Platform"))
        {
            player.transform.parent = null;
        }
    }

}
